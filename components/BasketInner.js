import styles from '../styles/Basket.module.css';


export function BasketInner(props) {

    return (

        <div className={styles.cart_item}>
            <div className={styles.cart_item__main}>
                <div className={styles.cart_item__start}>
                    <button className={styles.cart_item__btn} type="button">Del</button>
                </div>
                <div className={styles.cart_item__img_wrapper}>
                    <img className={styles.cart_item__img} src={props.image} alt="" />
                </div>
                <div className={styles.cart_item__content}>
                    <h3 className={styles.cart_item__title}>{props.title}</h3>
                </div>
            </div>
            <div className={styles.cart_item__end}>
                <div className={styles.cart_item__actions}>
                    <button className={styles.cart_item__btn} type="button">-</button>
                    <span className={styles.cart_item__quantity}></span>
                    <button className={styles.cart_item__btn} type="button">+</button>
                </div>
                <p className={styles.cart_item__price}>{props.price}</p>
            </div>
        </div>

    )



}
